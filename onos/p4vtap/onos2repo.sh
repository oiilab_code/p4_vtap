#!/bin/bash
#Author 1000rym
#Date:2017.11.07 
#Copy git managed project to onos project.
#Current Path: onos_dev(ONOS repository is on the same path with onos)
#ONOS Application path: ~/onos/apps/
path_onos_project=$(dirname "$ONOS_ROOT")
path_onos_dev=$path_onos_project/onos_dev
path_src_apps_p4vtap=$ONOS_ROOT/apps/p4vtap
path_dst_repo_p4vtap=$path_onos_dev/p4_vtap/onos


#Copy p4 vtap project to the ONOS application. 
echo "Copy onos p4vtap project to the repository."
rsync -rv $path_src_apps_p4vtap $path_dst_repo_p4vtap

echo "ONOS Project -> Git Repository done."
echo $path_dst_repo_p4vtap

