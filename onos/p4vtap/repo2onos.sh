#!/bin/bash
#Author 1000rym
#Date:2017.11.07 
#Copy git managed project to onos project.
#Current Path: onos_dev(ONOS repository is on the same path with onos)
#ONOS Application path: ~/onos/apps/
path_onos_project=$(dirname "$ONOS_ROOT")
path_onos_dev=$path_onos_project/onos_dev
path_src_p4_vtap=$path_onos_dev/p4_vtap/onos/p4vtap
path_src_modules=$path_onos_dev/p4_vtap/onos/modules.defs
path_dst_onos_apps=$ONOS_ROOT/apps
path_dst_modules=$ONOS_ROOT/


#Copy p4 vtap project to the ONOS application. 
echo "Copy p4vtap project to the onos(ignore modules.defs file)."
rsync -rv $path_src_p4_vtap --exclude=*.iml --exclude=.DS_Store $path_dst_onos_apps

echo "Copy Git Repository -> ONOS Project done."

