package org.onosproject.p4vtap.pipeconf;

import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Point;
import org.influxdb.dto.BatchPoints;

public class InfluxDBConnector {
    private static final String DB_URL = "http://52.78.224.174:8086";
    private static final String USER_ID="admin";
    private static final String PASSWD="admin";
    private static final String DB_NAME="bmv2_test1";
    private static final String TABLE_NAME="java_test3";
    private static final String SRC_IP="src_ip";
    private static final String DST_IP="dst_ip";
    private static final String PROTOCOL="proto";
    private static final String DEVICE_ID="device_id";
    private static final String APP_SIG="app_sig";
    private InfluxDB influxDB;
    private static InfluxDBConnector instance;

    private InfluxDBConnector(){
        influxDB = InfluxDBFactory.connect(DB_URL,USER_ID,PASSWD);
    }

    /**
     * Create singleton class to avoid generate multi instance;
     * @return
     */
    public static synchronized InfluxDBConnector getInstance(){
        if(instance == null){
            instance = new InfluxDBConnector();
        }
        return instance;
    }


    public void writeLevelOneQuer(String device_id, String proto, String src_ip, String dst_ip){
        BatchPoints batchPoints = BatchPoints.database(DB_NAME).build();

        Point point = Point.measurement(TABLE_NAME)
                .tag(DEVICE_ID,device_id)
                .tag(PROTOCOL,proto)
                .addField(SRC_IP,src_ip)
                .addField(DST_IP,dst_ip)
                .addField(PROTOCOL,proto)
                .build();

        batchPoints.point(point);
        influxDB.write(batchPoints);
    }

    public void writeLevelTwouery(String device_id, String appId, String proto, String src_ip, String dst_ip){
        BatchPoints batchPoints = BatchPoints.database(DB_NAME).build();

        Point point = Point.measurement(TABLE_NAME)
                .tag(DEVICE_ID,device_id)
                .tag(APP_SIG, appId)
                .tag(PROTOCOL,proto)
                .addField(SRC_IP,src_ip)
                .addField(DST_IP,dst_ip)
                .addField(PROTOCOL,proto)
                .build();

        batchPoints.point(point);
        influxDB.write(batchPoints);
    }
}
