package org.onosproject.p4vtap.rulemanager;

import org.apache.karaf.shell.commands.Command;
import org.apache.karaf.shell.commands.Option;
import org.onosproject.cli.AbstractShellCommand;

@Command(scope = "onos", name = "p4vtap", description = "P4 vtap Command Tool.")
public class CommandReceiver extends AbstractShellCommand{

    @Option(name = "-did", aliases = "--device_id", description = "The BMV2 Device ID",
            required = true, multiValued = false)
    String device_id = null;

    @Option(name = "-proto", aliases = "--protocol", description = "Protocol Information",
            required = false, multiValued = false)
    String protocol = null;

    @Option(name = "-sip", aliases = "--src_ip", description = "Source IP",
            required = false, multiValued = false)
    String src_ip = null;

    @Option(name = "-dip", aliases = "--dst_ip", description = "Destination IP",
            required = false, multiValued = false)
    String dst_ip = null;

    @Override
    protected void execute() {
        P4VtapRule newRule = new P4VtapRule();
        String logInfo= "Device:"+device_id + "\n";
        newRule.setDeviceId(device_id);


        if(protocol!=null) {
            logInfo += "Protocol:" + protocol + "\n";
            newRule.setProtocol(protocol);
        }

        if(src_ip!=null) {
            logInfo += "Source IP:" + src_ip + "\n";
            newRule.setSrcIP(src_ip);
        }

        if(dst_ip!=null) {
            logInfo += "Destination IP:" + dst_ip + "\n";
            newRule.setDstIP(dst_ip);
        }

        log.info("Install FlowRule->"+logInfo);
        get(P4VtapService.class).installRule(newRule);
    }
}
