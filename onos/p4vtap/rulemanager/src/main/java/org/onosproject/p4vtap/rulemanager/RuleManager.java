/*
 * Copyright 2017-present Open Networking Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.onosproject.p4vtap.rulemanager;

import org.apache.felix.scr.annotations.*;
import org.onosproject.app.ApplicationAdminService;
import org.onosproject.core.ApplicationId;
import org.onosproject.core.CoreService;
import org.onosproject.net.DeviceId;
import org.onosproject.net.device.DeviceService;
import org.onosproject.net.flow.DefaultFlowRule;
import org.onosproject.net.flow.DefaultTrafficSelector;
import org.onosproject.net.flow.DefaultTrafficTreatment;
import org.onosproject.net.flow.FlowRule;
import org.onosproject.net.flow.FlowRuleService;
import org.onosproject.net.flow.criteria.PiCriterion;
import org.onosproject.net.pi.runtime.PiAction;
import org.onosproject.net.pi.runtime.PiActionId;
import org.onosproject.net.pi.runtime.PiHeaderFieldId;
import org.onosproject.net.pi.runtime.PiPipeconfService;
import org.onosproject.net.pi.runtime.PiTableId;
import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * Simple application which manage flow rule set rule to bmv2.
 */
@Component(immediate = true)
@Service
public class RuleManager implements  P4VtapService{

    private static final Logger log = getLogger(RuleManager.class);

    private static final String APP_NAME = "org.onosproject.p4vtap.rulemanager";

    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    private DeviceService deviceService;

    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    private FlowRuleService flowRuleService;

    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    private ApplicationAdminService appService;

    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    private CoreService coreService;

    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    private PiPipeconfService piPipeconfService;
    private ApplicationId appId;

    private static final String IP_V4 = "ipv4";
    private static final String IP_PROTO_FILTER_TABLE = "ip_proto_filter_table";
    private static final PiHeaderFieldId PROTOCOL = PiHeaderFieldId.of(IP_V4, "protocol");
    private static final PiHeaderFieldId IPV4_SRC = PiHeaderFieldId.of(IP_V4, "src_addr");
    private static final PiHeaderFieldId IPV4_DST = PiHeaderFieldId.of(IP_V4, "dst_addr");
    private static final PiTableId IP_PROTO_FILTER_TABLE_ID = PiTableId.of(IP_PROTO_FILTER_TABLE);
    private static final PiActionId ACTION_CLONE = PiActionId.of("_clone");


    @Activate
    public void activate() {
        log.info("Starting...");
        appId = coreService.registerApplication(APP_NAME);
        // Install rules to existing devices.
        deviceService.getDevices().forEach(device -> log.info("Device:"+device.id(), appId.id()));

        log.info("STARTED", appId.id());
    }

    @Deactivate
    public void deactivate() {
        log.info("Stopping...");
        flowRuleService.removeFlowRulesById(appId);
        log.info("STOPPED");
    }

    @Override
    public void installRule(P4VtapRule p4VtapRule) {
        PiCriterion.Builder piCriterion = PiCriterion.builder();

        if(p4VtapRule.getProtocol()!=0){
           piCriterion.matchExact(PROTOCOL, p4VtapRule.getProtocol());
        }

        if(p4VtapRule.getSrcIP()!=0){
            piCriterion.matchLpm(IPV4_SRC,p4VtapRule.getSrcIP(),
                                 p4VtapRule.getSrcMask());
        }

        if(p4VtapRule.getDstIP()!=0){
            piCriterion.matchLpm(IPV4_DST, p4VtapRule.getDstIP(),
                                 p4VtapRule.getDstMask());
        }


        PiAction action_clone = PiAction.builder()
                .withId(ACTION_CLONE)
                .build();

        FlowRule flowRule = DefaultFlowRule.builder()
                .forDevice(p4VtapRule.getDevice())
                .forTable(IP_PROTO_FILTER_TABLE_ID) // ip_proto_clone_table
                .fromApp(appId)
                .makePermanent()
                .withPriority(1000)
                .withSelector(DefaultTrafficSelector.builder()
                                      .matchPi(piCriterion.build())
                                      .build())
                .withTreatment(
                        DefaultTrafficTreatment.builder()
                                .piTableAction(action_clone)
                                .build())
                .build();

        log.warn("Installing Clone rule to {}", p4VtapRule.getDevice());
        flowRuleService.applyFlowRules(flowRule);
    }

}
