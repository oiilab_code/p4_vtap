package org.onosproject.p4vtap.rulemanager;

import org.onosproject.net.DeviceId;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.math.BigInteger;


public class P4VtapRule {
    private DeviceId device;
    private byte protocol;
    private long srcIP;
    private int srcMask;
    private long dstIP;
    private int dstMask;

    public P4VtapRule(){
        this.device =  DeviceId.deviceId("unknown");
        this.protocol = (byte)0x00;
        this.srcIP = 0x00000000;
        this.srcMask = 0x0000;
        this.dstIP=0x00000000;
        this.dstMask = 0x0000;
    }

    public void setDeviceId(String deviceId){
        this.device = DeviceId.deviceId(deviceId);
    }

    public void setProtocol(String protocol){
        if(protocol.toLowerCase().equals("icmp"))
            this.protocol = (byte) 0x01;
        else if(protocol.toLowerCase().equals("tcp"))
            this.protocol = (byte) 0x06;
        else if(protocol.toLowerCase().equals("udp"))
            this.protocol = (byte) 0x17;
    }

    private long shieldsquare_IP2Hex(String reqIpAddr) {
        String hex = "";
        long value = 0;
        String[] part = reqIpAddr.split("[\\.,]");
        if (part.length < 4) {
            return value;
        }
        for (int i = 0; i < 4; i++) {
            int decimal = Integer.parseInt(part[i]);
            if (decimal < 16) // Append a 0 to maintian 2 digits for every
            // number
            {
                hex += "0" + String.format("%01X", decimal);
            } else {
                hex += String.format("%01X", decimal);
            }
        }
        value = new BigInteger(hex,16).longValue();
        return value;
    }

    public void setSrcIP(String ipWithMask){
        String[] parts = ipWithMask.split("/");
        String ip = parts[0];

        this.srcIP = shieldsquare_IP2Hex(ip);

        int prefix;
        if (parts.length < 2) {
            prefix = 0;
        } else {
            prefix = Integer.parseInt(parts[1]);
        }
        int mask = 0xffffffff << (32 - prefix);
        int value = mask;

        byte[] bytes = new byte[]{
                (byte)(value & 0xff),(byte)(value >> 8 & 0xff),(byte)(value >> 16 & 0xff),(byte)(value >>> 24)};

        try {
            InetAddress netAddr = InetAddress.getByAddress(bytes);
            String strmask = netAddr.getHostAddress();
            this.srcMask=(int)shieldsquare_IP2Hex(strmask);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    public void setDstIP(String ipWithMask){
        String[] parts = ipWithMask.split("/");
        String ip = parts[0];

        this.dstIP = shieldsquare_IP2Hex(ip);

        int prefix;
        if (parts.length < 2) {
            prefix = 0;
        } else {
            prefix = Integer.parseInt(parts[1]);
        }
        int mask = 0xffffffff << (32 - prefix);
        int value = mask;

        byte[] bytes = new byte[]{
                (byte)(value & 0xff),(byte)(value >> 8 & 0xff),(byte)(value >> 16 & 0xff),(byte)(value >>> 24)};

        try {
            InetAddress netAddr = InetAddress.getByAddress(bytes);
            String strmask = netAddr.getHostAddress();
            this.dstMask=(int)shieldsquare_IP2Hex(strmask);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    public DeviceId getDevice(){
        return this.device;
    }

    public byte getProtocol(){
        return this.protocol;
    }

    public long getSrcIP(){
        return this.srcIP;
    }

    public long getDstIP(){
        return  this.dstIP;
    }

    public int getSrcMask(){ return this.srcMask; }

    public int getDstMask(){
        return this.dstMask;
    }

}
