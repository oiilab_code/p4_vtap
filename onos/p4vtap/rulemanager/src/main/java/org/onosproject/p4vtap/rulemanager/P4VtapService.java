package org.onosproject.p4vtap.rulemanager;

/**
 * Created by cheonlim on 2017. 11. 15..
 */
public interface P4VtapService {
    /**
     * Add P4vtap Clone Rule.
     */
    void installRule(P4VtapRule p4vtapRule);
}
